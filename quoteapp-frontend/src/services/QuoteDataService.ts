import type { Quote } from "@/types/quote";
import { AbstractRestDataService } from "./AbstractRestDataService";

 class QuoteDataService extends AbstractRestDataService<Quote> {
  constructor() {
    {
      super(import.meta.env.VITE_API_URL, "quote");
    }
  }
}
export default new  QuoteDataService ()

