export class AbstractRestDataService<T> {
  constructor(protected baseUrl: string, protected entity: string) { 
  }
  
  async get(): Promise<T> {
    return await fetch(`${this.baseUrl}/${this.entity}`).then((response) =>
      response.json()
    );
  }

}