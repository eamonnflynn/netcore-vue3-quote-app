import QuoteDataService from "@/services/QuoteDataService";
import type { Quote } from "@/types/quote";
import { defineStore } from "pinia";

interface State {
  quote: Quote | null;
  loading: boolean;
  error: Error | null;
}

export const useQuoteStore = defineStore({
  id: "Quote",
  state: () =>
    ({
      quote: null,
      loading: false,
      error: null,
    } as State),

  getters: {
    getQuote: (state) => () => {
      return state.quote;
    },
  },

  actions: {
    async fetchQuote() {
      this.quote = null;
      this.loading = true;
      await QuoteDataService.get()
        .then((response) => (this.quote = response))
        .catch((error) => {
          this.error = error;
        })
        .finally(() => {
          this.loading = false;
        });
    },
  },
});
