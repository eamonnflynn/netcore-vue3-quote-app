﻿namespace QuoteApp.WebApi;

public interface IQuoteService
{
    Quote GetQuote();
}