﻿namespace QuoteApp.WebApi;

public class QuoteService : IQuoteService
{
    private readonly List<Quote> _quotes = new List<Quote>()
    {
        // get quote from yoda
        new Quote() { Text = "Do or do not. There is no try.", Person = "Yoda" },
        // get a quote from Mark Twain
        new Quote() { Text = "The secret of getting ahead is getting started.", Person = "Mark Twain" },
        // get q quote from Homer Simpson
        new Quote() { Text = "I am so smart! S-M-R-T! I mean S-M-A-R-T!", Person = "Homer Simpson" },
        // get a quote from Bill Gates
        new Quote() { Text = "Success is a lousy teacher. It seduces smart people into thinking they can't lose.", Person = "Bill Gates" },
        // get a quote from Steve jobs
        new Quote() { Text = "Stay hungry, stay foolish.", Person = "Steve Jobs" },
        // get a quote from Sheldon Cooper
        new Quote() { Text = "Bazinga!", Person = "Sheldon Cooper" },
        // get a quote from Winston Churchill
        new Quote() { Text = "Success consists of going from failure to failure without loss of enthusiasm.", Person = "Winston Churchill" },
        // get a quote from The Joker
        new Quote() { Text = "Why so serious?", Person = "The Joker" },
        // get a quote from the Terminator
        new Quote() { Text = "I'll be back.", Person = "The Terminator" },
        // get a quote from Hamlet
        new Quote() { Text = "To be, or not to be: that is the question.", Person = "Hamlet" },
    };
        
    public Quote GetQuote()
    {
        // get a random quote from _quotes
        var random = new Random();
        var index = random.Next(0, _quotes.Count);
        return _quotes[index];
    }
  
}